// O problema dessa abordagem é a mistura de elementos de negócio com os visuais.

var campos = [
    document.querySelector('#data'),
    document.querySelector('#quantidade'),
    document.querySelector('#valor')
]

console.log(campos);

var tbody = document.querySelector('table tbody'); // obtem o grid que o elemento ficará qdo salvo
document.querySelector('.form').addEventListener('submit', function (event) {
    event.preventDefault(); // aqui evita que a página seja recarregada no submit - que é o comportamento padrão
    
    var tr = document.createElement('tr'); 
    campos.forEach(function(campo) { //percorre os campos para ir criando os tds e colocando eles na tr
        var td = document.createElement('td');
        td.textContent = campo.value;
        tr.appendChild(td);
    });

    // calcula e cria a coluna volume
    var tdVolume = document.createElement('td');
    tdVolume.textContent = campos[1].value * campos[2].value;
    tr.appendChild(tdVolume);
    
    // adiciona a tr dentro do tbody
    tbody.appendChild(tr);

    // limpando os campos após adicionar. Não faz loop para conseguir customizar cada valor
    campos[0].value = '';
    campos[1].value = 1;
    campos[2].value = 0;
    
    campos[0].focus();
});