import {ListaNegociacoes} from '../models/ListaNegociacoes';
import {Mensagem} from '../models/Mensagem';
import {NegociacoesView} from '../views/NegociacoesView';
import {MensagemView} from '../views/MensagemView';
import {NegociacaoService} from '../services/NegociacaoService';
import {DateHelper} from '../helpers/DateHelper';
import {Bind} from '../helpers/Bind';
import {Negociacao} from '../models/Negociacao';

class NegociacaoController {
    
    // declaradas no constructor para evitar que toda vez busque os elementos no DOM.
    constructor() {
        
        // atribuiu a document.querySelector a uma variavel $
        let $ = document.querySelector.bind(document); // o bind faz com que o querySelector ainda fique associado ao document. Se nao tiver, dá erro.
        this._inputData = $('#data');
        this._inputQuantidade = $('#quantidade');
        this._inputValor = $('#valor');
        //this._listaNegociacoes = new ListaNegociacoes(this, function(model) { // sem uso de arrow function, tem q passar o contexto

        this._listaNegociacoes = new Bind(
            new ListaNegociacoes(),
            new NegociacoesView($('#negociacoesView')),
            'adiciona','esvazia', 'ordena', 'inverteOrdem' // devido ao ...props (rest operator) são recebidos como array
        );
        
        /* this._listaNegociacoes = new ListaNegociacoes(model => 
            this._negociacoesView.update(model)); */ // com arrow function, o this deixa de ser dinâmico. Ele sempre aponta para o local/escopo onde foi declarado.
        this._mensagem = new Bind(
            new Mensagem(),
            new MensagemView($('#mensagemView')),
            'texto'
        );

        this._ordemAtual = '';
        this._service = new NegociacaoService();
        this._init();
    }
    
    _init() {       
        
        this._service
            .lista()
            .then(negociacoes => 
                negociacoes.forEach(negociacao => 
                    this._listaNegociacoes.adiciona(negociacao))
            )
            .catch(erro => {                
                this._mensagem.texto = erro;
            });

        setInterval(() => {
            this.importaNegociacoes();
        }, 3000);
    }
    
    adiciona(event) {
       
        event.preventDefault();

        let negociacao = this._criaNegociacao();

        this._service
            .cadastra(negociacao)
            .then(mensagem => {
                this._listaNegociacoes.adiciona(negociacao);
                this._mensagem.texto = mensagem;
                this._limpaFormulario();
            })
            .catch(erro => this._mensagem.texto = erro);

        
        
        // console.log(typeof(this._inputDate.value)); // para descobrir o tipo de um dado
    }

    importaNegociacoes() {
        
        this._service.importa(this._listaNegociacoes.negociacoes)
            .then(negociacoes => {
                negociacoes                
                    .forEach(negociacao => this._listaNegociacoes.adiciona(negociacao));
                this._mensagem.texto = "Negociações importadas com sucesso!";
            })
            .catch(erro => this._mensagem.texto = erro);
    }

    apaga() {

        this._service
            .apaga()
            .then(mensagem => {
                this._mensagem.texto = mensagem;
                this._listaNegociacoes.esvazia();
            })
            .catch(erro => this._mensagem.texto = erro);

        //this._mensagem.texto = "Negociações apagadas com sucesso!";
    }
    
    _criaNegociacao() {
        
        return new Negociacao(
            DateHelper.textoParaData(this._inputData.value),
            parseInt(this._inputQuantidade.value),
            parseFloat(this._inputValor.value));    
    }
    
    _limpaFormulario() {
     
        this._inputData.value = '';
        this._inputQuantidade.value = 1;
        this._inputValor.value = 0.0;
        this._inputData.focus();   
    }

    ordena(coluna) {
        if (this._ordemAtual == coluna) {
            this._listaNegociacoes.inverteOrdem();
        } else {
            this._listaNegociacoes.ordena((a,b) => a[coluna] - b[coluna]);
        }
        this._ordemAtual = coluna;
    }
}

let negociacaoController = new NegociacaoController();

export function currentInstance() {
    return negociacaoController;
}