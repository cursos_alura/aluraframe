import {View} from './View';
import {DateHelper} from '../helpers/DateHelper';
import {currentInstance} from '../controllers/NegociacaoController';

export class NegociacoesView extends View {

    // Como é o unico construtor, ele pode ser suprimido sem problemas, pois herda da classe pai
    constructor(elemento) {
        super(elemento);
        
        elemento.addEventListener('click', function(event) {
            if (event.target.nodeName == 'TH') {
                currentInstance().ordena(event.target.textContent.toLowerCase());
            }
        })
    }

    template(model) {
        return `
        <table class="table table-hover table-bordered">
            <thead>
                <tr>
                    <th>DATA</th>
                    <th>QUANTIDADE</th>
                    <th>VALOR</th>
                    <th>VOLUME</th>
                </tr>
            </thead>
            
            <tbody>
                ${model.negociacoes.map(n => `
                    <tr>
                        <td>${DateHelper.dataParaTexto(n.data)}</td>
                        <td>${n.quantidade}</td>
                        <td>${n.valor}</td>
                        <td>${n.volume}</td>
                    </tr> 
                `).join('')}
            </tbody>
            
            <tfoot>
                <td colspan="3"></td>
                <td>                    
                    ${model.volumeTotal}
                </td>                
            </tfoot>
        </table>
        `;
        /* Ao contrário do Java, a seed do reduce é passada como 2º parametro. 
        Nesse exemplo é obrigatório passar uma seed = 0 para quando for renderizar 
        a tabela sem elementos. Caso não tenha o seed, ele vai reclamar que está 
        tentando fazer o reduce de um array vazio. */
    }

}