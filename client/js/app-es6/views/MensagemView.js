import {View} from './View';

export class MensagemView extends View {

    // Como é o unico construtor, ele pode ser suprimido sem problemas, pois herda da classe pai
    constructor(elemento) {
        super(elemento);
    }

    template(model) {
        return model.texto ? `<p class="alert alert-info"> ${model.texto} </p> ` : `<p/>`;
    }

}