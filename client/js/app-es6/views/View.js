// exporta a classe para ficar disponível para outras classes (qdo módulos do ES2015 estão ativados)
export class View {

    constructor(elemento) {
        this._elemento = elemento;
    }

    // Obriga que o desenvolvedor que extends View a implementar o método template
    template(model) {
        throw new Error("O método template deve ser implementado!");
    }

    update(model) {
        this._elemento.innerHTML = this.template(model);
    }
}