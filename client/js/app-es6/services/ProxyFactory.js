export class ProxyFactory {

    static create(objeto, props, acao) {
        return new Proxy(objeto, {
            
            // set quando quer interceptar uma escrita. Para uma leitura, seria get e n teria o paramentro value.
            get(target, prop, receiver) {
                
                /* internamente, quando o JS chama uma função, ele faz o get do nome da função e depois um Reflect.apply passando os parâmetros.
                 O segundo termo do if é para garantir que o adiciona ou esvazia interceptado é uma função.*/
                if (props.includes(prop) && ProxyFactory._ehFuncao(target[prop])) {
                    // Essa função substitui a adiciona() ou esvazia() no proxy.
                    return function() {
                        console.log(`${prop}`);
                        // arguments é uma variável implícita que da acesso aos parâmetros recebidos na função.
                        let retorno = Reflect.apply(target[prop], target, arguments);
                        return acao(target);
                        //return retorno;
                    }
                }
                return Reflect.get(target, prop, receiver);
                // Para verificar o valor antigo e o novo (somente no set)
                // console.log(`Valor novo: ${value} / Valor antigo: ${target[prop]}`);
            },
            set(target, prop, value, receiver) {
                if (props.includes(prop)) {
                    target[prop] = value;
                    acao(target);
                }
                return Reflect.set(target, prop, value, receiver);
            }
        });
    }

    static _ehFuncao(func) {
        return typeof(func) == typeof(Function);
    }

}