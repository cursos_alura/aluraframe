const stores = ['negociacoes'];
const version = 4;
const dbName = 'aluraframe';

let connection = null;
let close = null; // para guardar o comportamento padrão do close

export class ConnectionFactory {

    constructor() {
        throw new Error("Não é possível criar instâncias de ConnectionFactory");
    }

    static getConnection() {
        return new Promise((resolve, reject) => {

            let openRequest = window.indexedDB.open(dbName, version);

            openRequest.onupgradeneeded = e => {
                ConnectionFactory._createStores(e.target.result);
            };
            
            openRequest.onsuccess = e => {
                if (!connection) {
                    connection = e.target.result;
                    // com o bind, o this do close fica sendo a própria connection
                    close = connection.close.bind(connection); // guardando o comportamento padrão
                    // sobrescrevendo o método close para evitar que a conexão seja fechada pelo dev
                    connection.close = function() {
                        throw new Error("Você não pode fechar diretamente a conexão!");
                    };
                }
                resolve(connection);
            };
            
            openRequest.onerror = e => {
                console.log(e.target.error);
                reject(e.target.error.name);
            };                
        });
    }
    
    static _createStores(connection) {
        stores.forEach(store => {
            if (connection.objectStoreNames.contains(store)) {
                connection.deleteObjectStore(store);
            }
            connection.createObjectStore(store, {autoIncrement: true});
        });        
    }

    static closeConnection() {
        if (connection) {
            close(); // executa o comportamento armazenado em close
            connection = null;
        }
    }
}

