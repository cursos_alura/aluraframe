export class HttpService {
    getOLD_ES2015(url) {
        return new Promise((resolve, reject) => {
            let xhr = new XMLHttpRequest();
            xhr.open('GET', url);
    
            /* 
            onreadystatechange define o que será chamado a cada mudança de estado da xhr
            Estados possíveis de um requisição AJAX:
            0 - Requisição ainda não iniciada;
            1 - Conexão com o servidor estabelecida;
            2 - Requisição recebida;
            3 - Processando requisição;
            4 - Requisição concluída e a resposta está pronta.
            */
            xhr.onreadystatechange = () => {
                // Requisição concluída e a resposta está pronta.
                if (xhr.readyState == 4) {
                    // Erro também é uma resposta, por isso é necessário verificar o status 
                    if (xhr.status == 200) {
                        resolve(JSON.parse(xhr.responseText));
                    } else {                        
                        reject(xhr.responseText);
                    }
                }
            };
            
            xhr.send();
        });
    }

    get(url) {
        // para funcionar em qualquer navegador, foi adicionado o polyfill fetch.js
        return fetch(url)
            .then(resposta => this._handleErrors(resposta))
            .then(resposta => resposta.json());
        // qquer erro é capturado no catch de quem tá utilizando o HttpService.    
    }

    _handleErrors(resposta) {
        if (!resposta.ok) { // ok é status 200 (internamente)
            throw new Error(res.statusText);
        }
        return resposta;        
    }


    postOLD_ES2015(url, dado) {

        return new Promise((resolve, reject) => {

            let xhr = new XMLHttpRequest();
            xhr.open("POST", url, true);
            xhr.setRequestHeader("Content-type", "application/json");
            xhr.onreadystatechange = () => {

                if (xhr.readyState == 4) {
                    if (xhr.status == 200) {
                        resolve(JSON.parse(xhr.responseText));
                    } else {
                        reject(xhr.responseText);
                    }
                }
            };
            xhr.send(JSON.stringify(dado)); 
        });

    }

    post(url, dado) {
        // para funcionar em qualquer navegador, foi adicionado o polyfill fetch.js
        return fetch(url, {
            headers: {'Content-type' : 'application/json'}, 
            method: 'post',
            body: JSON.stringify(dado)
        })
        .then(resposta => this._handleErrors(resposta));
    }
}