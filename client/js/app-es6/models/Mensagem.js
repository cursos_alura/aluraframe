export class Mensagem {

    // definido um valor padrão para o texto (quando não for passado um customizado)
    constructor(texto='') {
        this._texto = texto;
    }

    get texto() {
        return this._texto;
    }

    set texto(texto) {
        this._texto = texto;
    }
}