export class ListaNegociacoes {
    
    constructor(contexto) {
        
        this._negociacoes = [];
        //this._armadilha = armadilha;
        //this._contexto = contexto; // para quando n usa arrow function (pra conseguir forçar o contexto do this)
    }
    
    adiciona(negociacao) {        
        this._negociacoes.push(negociacao);
        //this._armadilha(this); // apenas qdo usa arrow function
        //Reflect.apply(this._armadilha, this._contexto, this); // para quando n usa arrow function (pra conseguir forçar o contexto do this)
    }

    esvazia() {
        this._negociacoes = [];
        //this._armadilha(this); // apenas qdo usa arrow function
        //Reflect.apply(this._armadilha, this._contexto, this); // para quando n usa arrow function (pra conseguir forçar o contexto do this)
    }
    
    get negociacoes() {
        // retorna uma cópia da lista para evitar que ela seja alterada de forma indevida
        return [].concat(this._negociacoes);
    }

    get volumeTotal() {
        return this._negociacoes.reduce((total, n) => total + n.volume, 0.0);
    }

    ordena(criterio) {
        this._negociacoes.sort(criterio);
    }

    inverteOrdem() {
        this._negociacoes.reverse();
    }
}