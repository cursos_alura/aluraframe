export class Negociacao {
   
    // A declaração dos atributos da negociação é feita dentro do constructor
    constructor(data, quantidade, valor) {
        // o _ antes do valor serve para dizer q a propriedade só deve ser alterada de dentro da própria classe. Não impede, mas é uma convenção.
        this._data = new Date(data.getTime()); // evita que alterações na referência passada alterem o campo da classe.
        this._quantidade = quantidade;
        this._valor = valor;
        // Não impede que chame metodos de objetos que são atributos dessa classe. Ex: n1.data.setDate(11) funciona. Para evitar, retorna uma cópia da data.
        Object.freeze(this); // impede que os atributos sejam alterados após a criação do objeto.
    }
    
    /* Usar o get volume() ao inves de getVolume() permite usar o objeto.volume. Caso contrário, teria que usar objeto.getVolume().
     * Esse objeto.volume é apenas leitura, não tendo como atribuir valor. */
    get volume() {
        
        return this._quantidade * this._valor;
    }
    
    get data() {
        
        return new Date(this._data.getTime()); // evita que o atributo seja modificado
    }
    
    get quantidade() {
        
        return this._quantidade;
    }
    
    get valor() {
        
        return this._valor;
    }

    isEquals(outraNegociacao) {
        return JSON.stringify(this) == JSON.stringify(outraNegociacao);
    }
}