import {ProxyFactory} from '../services/ProxyFactory';

// Serve para configurar o Proxy e também já fazer a primeira chamada ao update()
export class Bind {

    constructor(model, view, ...props) {
        let proxy = ProxyFactory.create(model, props, (model) => view.update(model));
        view.update(model);
        return proxy;
    }
}