Project to apply new concepts of ES6.

Running the application:

1) Go to server directory and execute "npm start" to start the node server; <br />
2) Go to client directory, run the command "npm install" to install de project dependencies; <br />
3) Finally, execute the command "npm run watch". A browser window will open.